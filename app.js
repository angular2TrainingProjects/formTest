"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require("@angular/forms");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var platform_browser_1 = require("@angular/platform-browser");
var FormDec = (function () {
    function FormDec(fb) {
        this.myform = fb.group({
            'name': [' ', forms_1.Validators.required]
        });
    }
    FormDec.prototype.onSubmit = function (val) {
        console.log("this is:", val);
    };
    FormDec = __decorate([
        core_1.Component({
            selector: "form-dec",
            template: "\n\t<form [formGroup]=\"myform\" (ngSubmit)=\"onSubmit(myform.value)\" >\n\t<legend style=\"margin-bottom:4%;\">\u0627\u0637\u0644\u0627\u0639\u0627\u062A \u062E\u0648\u062F \u0631\u0627 \u0648\u0627\u0631\u062F \u0646\u0645\u0627\u06CC\u06CC\u062F:</legend>\n\t\t<div class=\"form-group\" [class.bg-danger]=\"!myform.controls['name'].valid && myform.controls['name'].touched\">\n\t\t\t<label for=\"name\">\u0646\u0627\u0645:  </label>\n\t\t\t<input type=\"text\" id=\"name\" placeholder=\"\u0646\u0627\u0645 \u062E\u0648\u062F \u0631\u0627 \u0648\u0627\u0631\u062F \u0646\u0645\u0627\u06CC\u06CC\u062F\" [formControl]=\"myform.controls['name']\" >\n\t\t\t<div *ngIf=\"!myform.controls['name'].valid\" class=\"alert alert-danger\">\n\t\t\t\t \u0646\u0627\u0645 \u0631\u0627 \u062E\u0627\u0644\u06CC \u06AF\u0630\u0627\u0634\u062A\u0647 \u0627\u06CC\u062F.\n\t\t\t\t <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\n\t\t\t </div>\n\t\t\t\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<input type=\"submit\" class=\"btn btn-primary\" value=\"\u0627\u0631\u0633\u0627\u0644\" />\n\t\t</div>\n\t</form> "
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder])
    ], FormDec);
    return FormDec;
}());
var FormDecModule = (function () {
    function FormDecModule() {
    }
    FormDecModule = __decorate([
        core_1.NgModule({
            declarations: [FormDec],
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, forms_1.ReactiveFormsModule],
            bootstrap: [FormDec]
        }), 
        __metadata('design:paramtypes', [])
    ], FormDecModule);
    return FormDecModule;
}());
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(FormDecModule);
//# sourceMappingURL=app.js.map