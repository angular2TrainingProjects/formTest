import {
	Component,
	NgModule
} from '@angular/core';
import {
	FormsModule,
	ReactiveFormsModule,
	FormGroup,
	FormBuilder,
	Validators
} from "@angular/forms";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { BrowserModule } from "@angular/platform-browser";
@Component({
	selector: "form-dec",
	template: `
	<form [formGroup]="myform" (ngSubmit)="onSubmit(myform.value)" >
	<legend style="margin-bottom:4%;">اطلاعات خود را وارد نمایید:</legend>
		<div class="form-group" [class.bg-danger]="!myform.controls['name'].valid && myform.controls['name'].touched">
			<label for="name">نام:  </label>
			<input type="text" id="name" placeholder="نام خود را وارد نمایید" [formControl]="myform.controls['name']" >
			<div *ngIf="!myform.controls['name'].valid" class="alert alert-danger">
				 نام را خالی گذاشته اید.
				 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			 </div>
			
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" value="ارسال" />
		</div>
	</form> `
})
class FormDec {
	myform: FormGroup;
	constructor(fb: FormBuilder) {
		this.myform = fb.group({
			'name': [' ' , Validators.required]
		});
	}
	onSubmit(val: string) {
		console.log("this is:",val);
	}
}
@NgModule({
	declarations: [ FormDec ] ,
	imports: [ BrowserModule,FormsModule,ReactiveFormsModule ] ,
	bootstrap: [ FormDec ]
})
class FormDecModule {}
platformBrowserDynamic().bootstrapModule(FormDecModule);